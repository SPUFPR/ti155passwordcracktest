<%-- 
    Document   : home
    Created on : 14/09/2018, 12:19:52
    Author     : Anselmo D. Adams [adams@eaatech.com.br]
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib uri="http://java.sun.com/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html lang="pt-BR">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <title>Atividade - quebra de senhas - TI155</title>
        <link rel="stylesheet" href="css/bootstrap.min.css"/>
    </head>
    <body>
        <nav class="navbar navbar-expand-lg navbar-dark bg-primary">
            <a class="navbar-brand" href="#">TI155 - Atividade Prática 1</a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>
        </nav>
        <div class="container-fluid">
            <div class="row">
                <div class="col-12">&nbsp;</div>
            </div>
            <div class="row">
                <div class="col-3">
                    <div class="card" style="width: 18rem;">
                        <div class="card-body">
                            <h5 class="card-title">Teste de Senha Numérica</h5>
                            <form>
                                <div class="form-group">
                                    <label for="npasslength">Comprimento da senha</label>
                                    <select id="npasslength" class="form-control">
                                        <option value="0">Selecione</option>
                                        <option value="4">4</option>
                                        <option value="5">5</option>
                                        <option value="6">6</option>
                                        <option value="7">7</option>
                                        <option value="8">8</option>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="npass">Senha</label>
                                    <input type="text" class="form-control" id="npass">
                                </div>
                            </form>
                            <input type="button" id="ntest" class="btn btn-primary" value="Iniciar Teste"/>
                        </div>
                    </div>
                </div>
                <div class="col-1">&nbsp;</div>
                <div class="col-3">
                    <div class="card" style="width: 18rem;">
                        <div class="card-body">
                            <h5 class="card-title">Teste de Dicionário</h5>
                            <form>
                                <div class="form-group">
                                    <label for="tpass">Senha</label>
                                    <input type="text" class="form-control" id="tpass">
                                </div>
                            </form>
                            <input type="button" id="dicttest" class="btn btn-primary" value="Iniciar Teste"/>
                        </div>
                    </div>
                </div>
                <div class="col-5">&nbsp;</div>
            </div>
            <div class="row">
                <div class="col-12">&nbsp;</div>
            </div>
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-body">
                            <h5 class="card-title">Teoria - Análise combinatória</h5>
                            <p class="card-text">
                                Exceto o caso de uso de dicionário para descoberta de senhas, os demais casos, que implicam na geração de sequências de caracteres, obedecem a fórmula de arranjos com repetições <i>A<sub>(n,p)</sub>) = n<sup>p</sup></i>, onde:
                            </p>
                            <p class="card-text">
                            <ul>
                                <li><b>n</b>: número de elementos possíveis</li>
                                <li><b>p</b>: número de elementos por agrupamento</li>
                            </ul>
                            </p>
                            <p class="card-text">
                                Neste caso, n é o número de caracteres possíveis, e p é o comprimento da senha. Em número de combinações possíveis, usando a tabela ASCII, para cada caso, temos a tabela abaixo:
                            </p>
                            <table class="table table-sm table-hover">
                                <thead class="thead-light">
                                    <tr>
                                        <th scope="col">Cj. Caracteres/Comp. Senha\(n. comb)</th>
                                        <th scope="col">p=1</th>
                                        <th scope="col">p=2</th>
                                        <th scope="col">p=3</th>
                                        <th scope="col">p=4</th>
                                        <th scope="col">p=5</th>
                                        <th scope="col">p=6</th>
                                        <th scope="col">p=7</th>
                                        <th scope="col">p=8</th>
                                        <th scope="col">p=9</th>
                                    </tr>
                                    <tr>
                                        <td>Digitos (n = 10)</td>
                                        <td>10</td>
                                        <td>100</td>
                                        <td>1.000</td>
                                        <td>10.000</td>
                                        <td>100.000</td>
                                        <td>1.000.000</td>
                                        <td>10.000.000</td>
                                        <td>100.000.000</td>
                                        <td>1.000.000.000</td>
                                    </tr>
                                    <tr>
                                        <td>Letras (sem case)<br/>(n = 26)</td>
                                        <td>26</td>
                                        <td>676</td>
                                        <td>17.576</td>
                                        <td>456.976</td>
                                        <td>11.881.376</td>
                                        <td>308.915.776</td>
                                        <td>8.031.810.176</td>
                                        <td>208.827.064.576</td>
                                        <td>5.429.503.678.976</td>
                                    </tr>
                                    <tr>
                                        <td>Letras/Digitos<br/>(sem case) (n = 36)</td>
                                        <td>36</td>
                                        <td>1.296</td>
                                        <td>46.656</td>
                                        <td>1.679.616</td>
                                        <td>60.466.176</td>
                                        <td>2.176.782.336</td>
                                        <td>78.364.164.096</td>
                                        <td>2.821.109.907.456</td>
                                        <td>101.559.956.668.416</td>
                                    </tr>
                                    <tr>
                                        <td>Digitos/Especiais<br/>(n = 42)</td>
                                        <td>42</td>
                                        <td>1.764</td>
                                        <td>74.088</td>
                                        <td>3.111.696</td>
                                        <td>130.691.232</td>
                                        <td>5.489.031.744</td>
                                        <td>230.539.333.248</td>
                                        <td>9.682.651.996.416</td>
                                        <td>406.671.383.849.472</td>
                                    </tr>
                                    <tr>
                                        <td>Letras (com case)<br/>(n = 52)</td>
                                        <td>52</td>
                                        <td>2.704</td>
                                        <td>140.608</td>
                                        <td>7.311.616</td>
                                        <td>380.204.032</td>
                                        <td>19.770.609.664</td>
                                        <td>1.028.071.702.528</td>
                                        <td>53.459.728.531.456</td>
                                        <td>2.779.905.883.635.712</td>
                                    </tr>
                                    <tr>
                                        <td>Letras/Especiais<br/>(sem case) (n = 58)</td>
                                        <td>58</td>
                                        <td>3.364</td>
                                        <td>195.112</td>
                                        <td>11.316.496</td>
                                        <td>656.356.768</td>
                                        <td>38.068.692.544</td>
                                        <td>2.207.984.167.552</td>
                                        <td>128.063.081.718.016</td>
                                        <td>7.427.658.739.644.928</td>
                                    </tr>
                                    <tr>
                                        <td>Letras/Digitos<br/>(com case) (n = 62)</td>
                                        <td>62</td>
                                        <td>3.844</td>
                                        <td>238.328</td>
                                        <td>14.776.336</td>
                                        <td>916.132.832</td>
                                        <td>56.800.235.584</td>
                                        <td>3.521.614.606.208</td>
                                        <td>218.340.105.584.896</td>
                                        <td>13.537.086.546.263.552</td>
                                    </tr>

                                    <tr>
                                        <td>Letras/Digitos/Espec.<br/>(sem case) (n = 68)</td>
                                        <td>68</td>
                                        <td>4.624</td>
                                        <td>314.432</td>
                                        <td>21.381.376</td>
                                        <td>1.453.933.568</td>
                                        <td>98.867.482.624</td>
                                        <td>6.722.988.818.432</td>
                                        <td>457.163.239.653.376</td>
                                        <td>31.087.100.296.429.568</td>
                                    </tr>
                                    <tr>
                                        <td>Letras/Especials<br/>(com case) (n = 84)</td>
                                        <td>84</td>
                                        <td>7.056</td>
                                        <td>592.704</td>
                                        <td>49.787.136</td>
                                        <td>4.182.119.424</td>
                                        <td>351.298.031.616</td>
                                        <td>29.509.034.655.744</td>
                                        <td>2.478.758.911.082.496</td>
                                        <td>208.215.748.530.929.664</td>
                                    </tr>
                                    <tr>
                                        <td>Letras/Digitos/Espec.<br/>(com case) (n = 94)</td>
                                        <td>94</td>
                                        <td>8.836</td>
                                        <td>830.584</td>
                                        <td>78.074.896</td>
                                        <td>7.339.040.224</td>
                                        <td>689.869.781.056</td>
                                        <td>64.847.759.419.264</td>
                                        <td>6.095.689.385.410.816</td>
                                        <td>572.994.802.228.616.704</td>
                                    </tr>
                                </thead>
                            </table>
                            <p class="card-text">
                                Se considerar um byte por caracter, apenas para p >= 5 que um computador comum não consegue gerar e armazenar um dicionário em memória com todas as combinações.
                            </p>
                        </div>
                    </div>

                </div>
            </div>
        </div>
        <div class="modal" tabindex="-1" role="dialog" style="width: 95rem  ;">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content">
                    <div class="modal-header">
                        <h5 class="modal-title">Saída - Teste</h5>
                        <!--<button type="button" class="close" data-dismiss="modal" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>//-->
                    </div>
                    <div class="modal-body">
                        <div style="overflow-x: auto; height: 65vh;" id="output"></div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" id="closetest">Fechar</button>
                    </div>
                </div>
            </div>
        </div>
        <!-- JavaScript //-->
        <script type="text/javascript" src="js/jquery.min.js"></script>
        <script type="text/javascript" src="js/jquery.mask.min.js"></script>
        <script type="text/javascript" src="js/popper.min.js"></script>
        <script type="text/javascript" src="js/bootstrap.min.js"></script>
        <script>
            function updateOutputPane(msg) {
                $('#output').append('<p>' + msg + '</p>');
                $('#output').animate({
                    scrollTop: $('#output > p:last').offset().top
                }, 200);
            }

            $(function () {
                var ws = null;
                $('#npasslength').change(function (e) {
                    var pl = parseInt($('#npasslength').val());
                    var mask = "";
                    for (i = 0; i < pl; ++i) {
                        mask += "9";
                    }
                    $('#npass').mask(mask).val("");
                });
                $('#npasslength').change();


                $('#ntest').click(function (e) {
                    if ($('#npasslength').val() == 0) {
                        alert('Selecione o comprimento da senha para teste!');
                        return;
                    }
                    if ($('#npass').val() == '') {
                        alert('Informe uma senha para teste!');
                        return;
                    }
                    var data = {
                        plength: parseInt($('#npasslength').val()),
                        password: $('#npass').val()
                    };
                    try {
                        var wsurl = window.location.href;
                        wsurl = wsurl.replace("http://", "ws://");
                        wsurl = wsurl.replace("https://", "wss://");
                        wsurl += "/test/numeric";
                        console.log(wsurl);
                        ws = new WebSocket(wsurl);
                        ws.onmessage = function (e) {
                            updateOutputPane(e.data);
                        };
                        ws.onopen = function (e) {
                            ws.send(JSON.stringify(data));
                        };
                        $('.modal').modal();
                        $('#output').empty();
                        updateOutputPane('<p>Iniciando teste para senha numérica (comprimento: ' + data.plength + ')</p>');
                    } catch (ex) {
                        alert(ex);
                    }
                });

                $('#dicttest').click(function (e) {
                    var opts = {
                        password: $('#tpass').val()
                    };

                    if (opts.password == '' || opts.password.length < 3 || opts.password.length > 16) {
                        alert('Informe uma senha para o teste de dicionário (entre 3 e 16 caracteres)');
                        return;
                    }
                    try {
                        var wsurl = window.location.href;
                        wsurl = wsurl.replace("http://", "ws://");
                        wsurl = wsurl.replace("https://", "wss://");
                        wsurl += "/test/dictionary";
                        ws = new WebSocket(wsurl);
                        ws.onmessage = function (e) {
                            updateOutputPane(e.data);
                        };
                        ws.onopen = function (e) {
                            ws.send(JSON.stringify(opts));
                        };
                        $('.modal').modal();
                        $('#output').empty();
                        updateOutputPane('<p>Iniciando teste de dicionário (usando lista com 1.2 bilhões de senhas conhecidas)</p>');
                    } catch (ex) {
                        alert(ex);
                    }
                });


                $('#closetest').click(function (e) {
                    if (ws != null) {
                        ws.close();
                        ws = null;
                    }
                    $('.modal').modal('hide');
                });
            });
        </script>
    </body>
</html>
