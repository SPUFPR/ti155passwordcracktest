/*
 * Cracking passwords assignment
 * This code was made as part of academic evaluation 
 * Don't copy if you have a similar assignment, but feel free to look around
 * for ideas. 
 * Student: Anselmo Daniel Adams - GRR20145552
 * Universidade Federal do Paraná (UFPR) - http://www.ufpr.br
 * Setor de Educação Profissional e Tecnológica (SEPT) - http://www.sept.ufpr.br
 * Tecnologia em Análise e Desenvolvimento de Sistemas (TADS) - http://www.tads.ufpr.br
 *  TI155 - Auditoria e Segurança de Sistemas
 *  2018/2 - Noturno
 */
package br.ufpr.ti155.crackpassword.tests;

import com.google.gson.Gson;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import javax.websocket.OnClose;
import javax.websocket.OnError;
import javax.websocket.OnMessage;
import javax.websocket.OnOpen;
import javax.websocket.Session;
import javax.websocket.server.ServerEndpoint;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Websocket server for Numeric Password Cracking routine
 *
 * @author Anselmo D. Adams [adams@eaatech.com.br]
 */
@ServerEndpoint("/test/numeric")
public class NumericPasswordTest {

    /**
     * Websocket Session
     */
    private Session wsSession;
    /**
     * Worker reference
     */
    private Thread worker;
    /**
     * Gson JSON serializer/deserializer
     */
    private Gson gson;
    /**
     * Logger (SLF4J)
     */
    private static Logger logger = LoggerFactory.getLogger("NumericTestLogger");

    /**
     * Process onOpen event
     *
     * @param session Websocket session
     */
    @OnOpen
    public void onOpen(Session session) {
        this.wsSession = session;
        this.gson = new Gson();
        logger.info("Websocket Session started");
        this.wsSession.getAsyncRemote().sendText("Websocket aberto");
    }

    /**
     * Process onClose event
     *
     * @param session Websocket session
     */
    @OnClose
    public void onClose(Session session) {
        logger.info("Websocket Session closed, stopping worker");
        stopWorker();
    }

    /**
     * Process onError event
     *
     * @param session Websocket session
     * @param thr Throwable
     */
    @OnError
    public void onError(Session session, Throwable thr) {
        logger.error("Websocket error", thr);
        stopWorker();
    }

    /**
     * Process onMessage event
     *
     * @param message Json/String received
     * @param session Websocket session
     */
    @OnMessage
    public void onMessage(String message, Session session) {
        logger.info("Websocket onMessage event, processing (start a worker if it isn't started already)");
        startWorker(gson.fromJson(message, JsonElement.class));
    }

    private void startWorker(JsonElement json) {
        if (worker == null) {
            JsonObject jobj = json.getAsJsonObject();
            Integer plength = jobj.get("plength").getAsInt();
            String password = jobj.get("password").getAsString();
            logger.info("Starting background worker");
            worker = new NumericPasswordTestWorker(plength, password, wsSession);
            worker.start();
        }
    }

    private void stopWorker() {
        if (worker != null) {
            logger.info("Stopping background worker");
            worker.interrupt();
        } else {
            logger.info("No worker active, ignoring");
        }
    }
}
