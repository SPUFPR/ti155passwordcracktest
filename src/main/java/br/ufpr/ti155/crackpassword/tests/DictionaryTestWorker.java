/*
 * Cracking passwords assignment
 * This code was made as part of academic evaluation 
 * Don't copy if you have a similar assignment, but feel free to look around
 * for ideas. 
 * Student: Anselmo Daniel Adams - GRR20145552
 * Universidade Federal do Paraná (UFPR) - http://www.ufpr.br
 * Setor de Educação Profissional e Tecnológica (SEPT) - http://www.sept.ufpr.br
 * Tecnologia em Análise e Desenvolvimento de Sistemas (TADS) - http://www.tads.ufpr.br
 *  TI155 - Auditoria e Segurança de Sistemas
 *  2018/2 - Noturno
 */
package br.ufpr.ti155.crackpassword.tests;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.InputStreamReader;
import java.time.Duration;
import java.time.LocalDateTime;
import javax.websocket.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.tukaani.xz.XZInputStream;

/**
 * Worker for Dictionary password guessing
 *
 * @author Anselmo D. Adams [adams@eaatech.com.br]
 */
public class DictionaryTestWorker extends Thread {

    private String password;
    private Session session;
    private boolean foundMatch = false;
    private static final Logger logger = LoggerFactory.getLogger("DictionaryTestWorker");
    private final LocalDateTime startTime;
    private ThreadGroup tg;
    private final File[] dictionaries = {
        new File("/mnt/disk1/pdict/xaa"),
        new File("/mnt/disk1/pdict/xab"),
        new File("/mnt/disk1/pdict/xac"),
        new File("/mnt/disk1/pdict/xad"),
        new File("/mnt/disk1/pdict/xae"),
        new File("/mnt/disk1/pdict/xaf")
    };

    public DictionaryTestWorker(String password, Session session) {
        this.password = password;
        this.session = session;
        this.startTime = LocalDateTime.now();
        this.tg = new ThreadGroup("DictionaryTestWorker");
        this.setName("DictionaryTestWorker");
        this.setDaemon(true);
    }

    private void sendText(String msg) {
        if (msg != null) {
            try {
                this.session.getAsyncRemote().sendText(msg);
            } catch (Exception ex) {
            }
        }
    }

    @Override
    public void run() {
        logger.info("Main worker starting subworkers");
        sendText("Iniciando subworkers");
        for (int i = 0; i < dictionaries.length; ++i) {
            startWorker(i);
        }
        while (!Thread.currentThread().interrupted()) {
        }
        if (!foundMatch) {
            sendText("Senha não encontrada (processo interrompido/não encontrado no dicionário)");
            Duration duration = Duration.between(startTime, LocalDateTime.now());
            String status = "Duração total (segundos): ";
            if (duration.getSeconds() < 1) {
                status += "" + duration.getNano() / 1000000000;
            } else {
                status = "" + duration.getSeconds();
            }
            sendText(status);
        }
        logger.info("Interrupting threads");
        try {
            this.tg.interrupt();
        } catch (Exception ex) {
        }
        logger.info("Worker exited");

    }

    @Override
    public synchronized void start() {
        super.start();
    }

    private synchronized void stopJob() {
        try {
            this.tg.interrupt();
        } catch (Exception ex) {
            logger.error("Exception", ex);
        }
    }

    private synchronized void setFound() {
        this.foundMatch = true;
    }

    public void startWorker(final int idx) {
        Thread t = new Thread(this.tg, new Runnable() {
            @Override
            public void run() {
                logger.info("Starting search thread (dictionary file {})", dictionaries[idx].getName());
                sendText("Iniciando worker " + Thread.currentThread().getName());
                BufferedReader br = null;
                try {
                    br = new BufferedReader(new FileReader(dictionaries[idx]));
                } catch (Exception ex) {
                }
                if (br != null) {
                    boolean found = false;
                    while (!Thread.currentThread().interrupted()) {
                        String line = null;
                        try {
                            while ((line = br.readLine()) != null) {
                                line = line.replace("\n", "").replace("\r", "");
                                if (Thread.currentThread().interrupted()) {
                                    break;
                                }
                                if (password.equals(line)) {
                                    logger.info("Senha encontrada: {}", line);
                                    found = true;
                                    setFound();
                                    StringBuilder sb = new StringBuilder();
                                    sb.append("Senha descoberta - valor fornecido: ").append(password).append(", valor encontrado ").append(line);
                                    Duration duration = Duration.between(startTime, LocalDateTime.now());
                                    if (duration.getSeconds() < 1) {
                                        sb.append("<br/>Duração (segundos): ").append(new Double(duration.getNano() / 1000000000.0));
                                    } else {
                                        sb.append("<br/>Duração (segundos): ").append(duration.getSeconds());
                                    }
                                    sendText(sb.toString());
                                    break;
                                }
                            }
                        } catch (Exception ex) {
                        }
                        if (found) {
                            sendText("Senha encontrada [ worker " + Thread.currentThread().getName() + "]");
                            logger.info("Stopping all jobs");
                            stopJob();
                        }
                        break;
                    }
                }

                sendText("Worker " + Thread.currentThread().getName() + " finalizado");
                logger.info("Thread exiting {}", Thread.currentThread().getName());
            }
        });
        t.setName(this.tg.getName() + "-" + t.getId() + "-" + dictionaries[idx].getName());
        t.start();
    }
}
