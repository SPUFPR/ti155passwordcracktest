/*
 * Cracking passwords assignment
 * This code was made as part of academic evaluation 
 * Don't copy if you have a similar assignment, but feel free to look around
 * for ideas. 
 * Student: Anselmo Daniel Adams - GRR20145552
 * Universidade Federal do Paraná (UFPR) - http://www.ufpr.br
 * Setor de Educação Profissional e Tecnológica (SEPT) - http://www.sept.ufpr.br
 * Tecnologia em Análise e Desenvolvimento de Sistemas (TADS) - http://www.tads.ufpr.br
 *  TI155 - Auditoria e Segurança de Sistemas
 *  2018/2 - Noturno
 */
package br.ufpr.ti155.crackpassword.tests;

import java.time.Duration;
import java.time.LocalDateTime;
import javax.websocket.Session;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Worker for numeric password guessing
 *
 * @author Anselmo D. Adams [adams@eaatech.com.br]
 */
public class NumericPasswordTestWorker extends Thread {

    private Integer plength, middle, max;
    private String password, pFormat;
    private final LocalDateTime startTime;
    private final Session session;
    private static final Logger logger = LoggerFactory.getLogger("NumericPasswordTestWorker");
    private ThreadGroup tg;

    public NumericPasswordTestWorker(Integer plength, String password, Session session) {
        this.plength = plength;
        this.password = password;
        this.session = session;
        this.max = new Double(Math.pow(10.0, new Integer(this.plength).doubleValue())).intValue() - 1;
        this.middle = this.max / 2 + 1;
        this.setDaemon(true);
        this.setName("NumericTestWorker");
        this.pFormat = "%0" + this.plength + "d";
        this.startTime = LocalDateTime.now();
        this.tg = new ThreadGroup("NumericTestWorker");
        this.tg.setDaemon(true);
    }

    @Override
    public synchronized void start() {
        super.start();
        logger.info("Starting worker for  " + plength + " numeric password length, min = 0, max = " + max);
        sendText("Iniciando worker");
    }

    private void sendText(String msg) {
        if (msg != null) {
            try {
                this.session.getAsyncRemote().sendText(msg);
            } catch (Exception ex) {
                logger.error("Exception", ex);
            }
        }
    }

    private synchronized void stopJob() {
        try {
            this.tg.interrupt();
        } catch (Exception ex) {
            logger.error("Exception", ex);
        }
    }

    @Override
    public void run() {
        logger.info("Main worker starting subworkers");
        sendText("Iniciando subworkers (4 threads)");
        new Thread(this.tg, getSubWorker(0, middle / 2, true)).start();
        new Thread(this.tg, getSubWorker(middle / 2 + 1, middle, true)).start();
        new Thread(this.tg, getSubWorker(middle + 1, middle + middle / 2, false)).start();
        new Thread(this.tg, getSubWorker(middle + middle / 2 + 1, max, false)).start();
        while (!Thread.interrupted()) {
        }
        logger.info("Interrupting threads");
        stopJob();
        logger.info("Worker exited");
    }

    private Runnable getSubWorker(final int from, final int to, final boolean desc) {
        return new Runnable() {
            @Override
            public void run() {
                Thread.currentThread().setName("NumericTestWorker-" + Thread.currentThread().getId());
                logger.info("Starting search thread (from {}, to {}, descending {})", from, to, desc);
                sendText("Iniciando worker " + Thread.currentThread().getName());
                while (!Thread.currentThread().interrupted()) {
                    boolean found = false;
                    if (desc) {
                        for (int i = to; i >= from; --i) {
                            String tmp = String.format(pFormat, i);
                            if (!Thread.currentThread().interrupted()) {
                                if (password.equals(tmp)) {
                                    logger.info("Senha encontrada: {}", tmp);
                                    found = true;
                                    StringBuilder sb = new StringBuilder();
                                    sb.append("Senha descoberta - valor fornecido: ").append(password).append(", valor encontrado ").append(tmp);
                                    Duration duration = Duration.between(startTime, LocalDateTime.now());
                                    if (duration.getSeconds() < 1) {
                                        sb.append("<br/>Duração (segundos): ").append(new Double(duration.getNano() / 1000000000.0));
                                    } else {
                                        sb.append("<br/>Duração (segundos): ").append(duration.getSeconds());
                                    }
                                    sendText(sb.toString());
                                    break;
                                }
                            } else {
                                break;
                            }
                        }
                    } else {
                        for (int i = from; i <= to; ++i) {
                            String tmp = String.format(pFormat, i);
                            if (!Thread.currentThread().interrupted()) {
                                if (password.equals(tmp)) {
                                    logger.info("Senha encontrada: {}", tmp);
                                    found = true;
                                    StringBuilder sb = new StringBuilder();
                                    sb.append("Senha descoberta - valor fornecido: ").append(password).append(", valor encontrado ").append(tmp);
                                    Duration duration = Duration.between(startTime, LocalDateTime.now());
                                    if (duration.getSeconds() < 1) {
                                        sb.append("<br/>Duração (segundos): ").append(new Double(duration.getNano() / 1000000000.0));
                                    } else {
                                        sb.append("<br/>Duração (segundos): ").append(duration.getSeconds());
                                    }
                                    sendText(sb.toString());
                                    break;
                                }
                            } else {
                                break;
                            }
                        }
                    }

                    if (found) {
                        sendText("Senha encontrada [ worker " + Thread.currentThread().getName() + "]");
                        logger.info("Stopping all jobs");
                        stopJob();
                    }
                    break;
                }
                sendText("Worker " + Thread.currentThread().getName() + " finalizado");
                logger.info("Thread exiting {}", Thread.currentThread().getName());
            }
        };
    }

}
