/*
 * Cracking passwords assignment
 * This code was made as part of academic evaluation 
 * Don't copy if you have a similar assignment, but feel free to look around
 * for ideas. 
 * Student: Anselmo Daniel Adams - GRR20145552
 * Universidade Federal do Paraná (UFPR) - http://www.ufpr.br
 * Setor de Educação Profissional e Tecnológica (SEPT) - http://www.sept.ufpr.br
 * Tecnologia em Análise e Desenvolvimento de Sistemas (TADS) - http://www.tads.ufpr.br
 *  TI155 - Auditoria e Segurança de Sistemas
 *  2018/2 - Noturno
 */
package br.ufpr.ti155.crackpassword.controllers;

import br.com.caelum.vraptor.Controller;
import br.com.caelum.vraptor.Get;
import br.com.caelum.vraptor.Result;
import javax.inject.Inject;
import javax.servlet.http.HttpServletRequest;

/**
 * VRaptor controller - show main screen
 *
 * @author Anselmo D. Adams [adams@eaatech.com.br]
 */
@Controller
public class HomeController {

    @Inject
    private HttpServletRequest request;

    @Inject
    private Result result;

    public HomeController() {
    }

    @Get("/")
    public void home() {
    }
}
